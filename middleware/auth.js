const jwt = require('jsonwebtoken')

module.exports = function auth(req, res, next){
    const authHeader = req.headers.authorization;
    //const {email,password} = req.body
    //users[email].refreshToken
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        try{
            const decoded =jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
            
            if(!decoded.email)
              return error
            req.user = decoded
            next()
        }
        catch(error){
            console.log(error)
            return res.status(400).json({
            err: 'Invaild token'
            })
        }
        
    } else {
        res.sendStatus(401);
    }
  };