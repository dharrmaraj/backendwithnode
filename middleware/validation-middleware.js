// src/middleware/validation-middleware.js

const validator = require('../helpers/validate');

const signup = (req, res, next) => {
    const validationRule = {
        "email": "required|email",
        "password": "required|string|min:6",
        "username": "required|string",
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412)
                .send({
                    success: false,
                    message: err,
                    data: []
                });
        } else {
            next();
        }
    });
}

module.exports = { 
  signup
}