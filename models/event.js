const mongoose = require('mongoose');

Schema = mongoose.Schema;

const EventSchema = new Schema({
    event_name: {
        type: String,
        required: true,
        unique: true
    },
    event_image: {
        type: String,
        required: false,
    },
    from_date: {
        type: Date,
        required: true,
    },
    to_date: {
        type: Date,
        required: true,
    },
    timing1: {
        type: Date,
        required: true,
    },
    timing2: {
        type: Date,
        required: true,
    },
    hallname: {
        type: String,
        required: true
    },
    venue: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    user:{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

exports.Event = mongoose.model('Event', EventSchema);