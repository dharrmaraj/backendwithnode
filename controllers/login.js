const jwt = require('jsonwebtoken')
const { User } = require('../models/user');
// let users = [
//     {"email":"dharmaraj@squashapps.com","password":"123456","refreshToken":""},
//     {"email":"sathishkumar@squashapps.com","password":"123456","refreshToken":""}
// ]

async function login(req,res){
  try{
    const {email,password} = req.body
   
    //console.log(users[email])
    // if (!email || !password || users[email] !== email && users[email]['password'] !== password){
    //     return res.status(401).send()
    // }

    // const user = users.find(u => { return u.email === email && u.password === password });
    // const index = users.findIndex(u => u.email === email && u.password === password);
    // if(!user){
    //     return res.status(401).send()  
    // }
    const story = await User.findOne({email: email}).populate('events')
    console.log(story,"userpop");
    User.findOne({
        email: email
    }, async function(err, user) {
       
        if (err) throw err;
        
        var awaitPassword = await user.comparePassword(password);
        if (!user || ! awaitPassword || !password) {
          return res.status(200).send({ success: false, message: 'Authentication failed. Invalid email or password.' });
        }


        //use the payload to store information about the user such as username, user role, etc.
        let payload = {"email": user.email}

        //create the access token with the shorter lifespan
        let accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
            algorithm: "HS256",
            //expiresIn: process.env.ACCESS_TOKEN_LIFE
        })

        //create the refresh token with the longer lifespan
        let refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
            algorithm: "HS256",
            //expiresIn: process.env.REFRESH_TOKEN_LIFE
        })
        
        //res.cookie("jwt", accessToken, {secure: true, httpOnly: true})
        return res.status(200).send({
            success: true,
            message: 'User loggedin successfully',
            data: {accessToken,refreshToken, _id:user._id}
        })

        //return res.json({ token: jwt.sign({ email: user.email, fullName: user.fullName, _id: user._id }, 'RESTFULAPIs') });
    });


    //store the refresh token in the user array
    //users[index].refreshToken = refreshToken

    //send the access token to the client inside a cookie
    
  } 
  catch(error){
   console.log(error)
  } 
    
};

module.exports = login;