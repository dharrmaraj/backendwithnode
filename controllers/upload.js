const { Upload } = require('../models/upload');
const { Event } = require('../models/event');
const fileFilter = (req,file,cb) =>{
    const allowedTypes = ["image/jpeg","image/jpg","image/png"]
    if(!allowedTypes.includes(file.mimetype)){
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error,false)
    }

    cb(null,true)
}


async function uploadFile(req,res){
 
  let uploadData = new Upload({
    image_name:req.file.path,
    id:req.body._id,
    file_path:req.file.filename
  })
  try{
    const ifexists = await Upload.findOne({id:req.body._id});
    console.log(ifexists,"exists")
    uploadData.save(async(saveErr) => {
      if(saveErr) {
          //console.log(saveErr,"error")
          // console.log(uploadData,"data")
          return res.status(200).send({
            success: false,
            message: saveErr.message,
            data:[]
          })
          
      }
     // console.log(uploadData)
      let accessEventname = "http://localhost:3000/"+''+uploadData.file_path+'';

      try{
        Event.updateOne({_id:req.body._id},{$set:{
          event_image: accessEventname,
        }},{$upsert:true},function(err,events){
         
          if(err){
              return res.status(200).send({  
                  success: false,
                  message: err.message,
                  data:[]
              })
          }
          return res.status(200).send({
            success: true,
            message: 'Image Uploaded successfully',
            data: Event
          });
      })
    }
    catch(error){
      console.log(error);
    }
      
    });
    
    
  }
  catch(error){   
    console.log(error);
  } 
  
}



module.exports = { uploadFile,fileFilter }