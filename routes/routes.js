const express = require('express'),
router = express.Router(),

login = require('../controllers/login'),

register = require('../controllers/register'),

users = require('../controllers/users'),

{ getEvents,addAndUpdateEvents,deleteEvents } = require('../controllers/events'),

{ uploadFile,fileFilter } = require('../controllers/upload'),

auth = require('../middleware/auth'),

validation = require('../middleware/validation-middleware');

multer = require('multer');

const storage = multer.diskStorage({
    destination:function(req,file,callback) {
        console.log(file,"file")
      callback(null,'./uploads/');
    },
    filename:function(req,file,callback) {
      callback(null,Date.now() + file.originalname);
    }
  });
  
  const upload = multer({
    storage: storage,
    
    limits: {
      fileSize: 500000
    },
    fileFilter:fileFilter
  })

router.get('/',(req, res) => {
    res.send('Hello World!')
})

router.post('/login',login)

router.post('/register',validation.signup,register)

router.get('/users',auth,users)

router.get('/getevents/:id',auth,getEvents)

router.post('/addandupdateevent',auth,addAndUpdateEvents)

router.delete('/deleteevent/:id',auth,deleteEvents)

router.post('/upload',upload.single('event_image'),auth,uploadFile)

module.exports = router