const { Event } = require('../models/event');
const { User } = require('../models/user');
const jwt = require('jsonwebtoken')

async function getEvents(req,res){
 try{
    //console.log(req.params,"paramsget")
  
    Event.find({user:req.params.id}, function(err, events) {
       
        if (err){
            res.status(200).send({  
                success: false,
                message: err.message,
                data:[]
            })
        }
    
        return res.status(200).send({"success": true, "message": 'Events List',"Events":events});  
    });
   
 }
 catch(error){
  console.log(error)
 }
    
};

async function addAndUpdateEvents(req,res){
    try{
       const { data,flag  } = req.body;
        
       if(flag=='add'){
        let model = new Event({
            event_name: data.event_name,
            event_image: "",
            from_date: data.from_date,
            to_date: data.to_date,
            timing1: data.timing1,
            timing2: data.timing2,
            hallname: data.hallname,
            venue: data.venue,
            description: data.description,
            user:data.user
          })
          model.save(async (saveErr)=>{
            if(saveErr){
                res.status(200).send({  
                    success: false,
                    message: saveErr.message,
                    data:[]
                })
            }

            const user = await User.findOne({_id:data.user});
            user.events.push(model._id);
            await user.save();

            return res.status(200).send({
                success: true,
                message: 'Event added successfully',
                data: model
            });
          })
       }else{
         Event.updateOne({_id:data._id},{$set:{
            event_name: data.event_name,
            //event_image: data.event_image,
            from_date: data.from_date,
            to_date: data.to_date,
            timing1: data.timing1,
            timing2: data.timing2,
            hallname: data.hallname,
            venue: data.venue,
            description: data.description,
            user:data.user
          }},{$upsert:true},function(err,events){
             
            if(err){
                res.status(200).send({  
                    success: false,
                    message: err.message,
                    data:[]
                })
            }
            return res.status(200).send({
                success: true,
                message: 'Event Updated successfully',
                data: {
                    _id:data._id,
                    event_image: data.event_image
                }
            });
         })
       }
    }
    catch(error){
     console.log(error)
    }
       
};

async function deleteEvents(req,res){
    try{
        const { id } = req.params;
        
        
      
       Event.findOneAndDelete({"event_name":id}, async function(err, events) {
           if (err){
             return res.status(200).send({  
                success: false,
                message: err,
                data:[]
                })
           }   
           
           var authorization = req.headers.authorization.split(' ')[1];

           var decoded = jwt.verify(authorization, process.env.ACCESS_TOKEN_SECRET);
           
           console.log(decoded)

           const user = await User.findOne({email:decoded.email});  
           console.log(user)
           await User.updateOne({email:decoded.email},
            { $pull: { 'events':  id} });
            console.log(`${User.updateOne,({_id:user._id},
                { $pull: { 'events':  id} })}`)
           return res.status(200).send({"success": true, "message": 'Events deleted successfully',"Events":events});  
       });
    }
    catch(error){
     console.log(error)
    }
       
};

module.exports = {getEvents,addAndUpdateEvents,deleteEvents};