const { User } = require('../models/user');
const bcrypt = require('bcrypt');

async function register(req,res){
    try{ 
        // const {email,password,username} = req.body
        // console.log(email,password,username);
        // res.send({email,password,username})

        // First Validate The Request
            // const { error } = validate(req.body);
            // if (error) {
            //     return res.status(400).send(error.details[0].message);
            // }

            // Check if this user already exisits
            let user = await User.findOne({ email: req.body.email });
            if (user) {
                return res.status(200).send({success: false,message:'That user already exisits!',data:[]});
            } else {
                // Insert the new user if they do not exist yet
                user = new User({
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.password
                });
                const salt = await bcrypt.genSalt(10);
                user.password = await bcrypt.hash(user.password, salt);
                user.save(async(saveErr) => {
                    if(saveErr) {
                        return res.status(200).send({
                            success: false,
                            message: saveErr.message,
                            data:[]
                        })
                    }
                    // const salt = await bcrypt.genSalt(10);
                    // user.password = await bcrypt.hash(user.password, salt);
                    return res.status(200).send({
                        success: true,
                        message: 'User registered successfully',
                        data: user
                    });
                });
            }
    } 
    catch(error){
        console.log(error)
    }
};

module.exports = register;