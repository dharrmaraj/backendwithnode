const Joi = require('joi');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');


Schema = mongoose.Schema;
const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        // minlength: 3,
        // maxlength: 50
    },
    email: {
        type: String,
        required: true,
        // minlength: 3,
        // maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        // minlength: 3,
        // maxlength: 1024
    },
    events:[{
        type: Schema.Types.ObjectId,
        ref: 'Event'
    }]
});

UserSchema.methods.comparePassword = function(passwordin) {
    console.log(passwordin, this.password)
    return  bcrypt.compareSync(passwordin, this.password)  //bcrypt.compareSync(passwordin, this.password)
};

// function validateUser(user) {
//     const schema = {
//         name: Joi.string().min(3).max(50).required(),
//         email: Joi.string().min(3).max(255).required().email(),
//         password: Joi.string().min(3).max(255).required()
//     };
//     return schema.validate(user, schema);
// }

exports.User = mongoose.model('User', UserSchema);
//exports.validate = validateUser;