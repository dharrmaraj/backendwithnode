const express = require('express'),
app = express(),
cors = require("cors"),
Joi = require('joi'),
multer = require('multer')
path = require('path');
dotenv = require("dotenv");
dotenv.config();
const mongoose = require('mongoose');
const fs = require('fs')

// app.get('/', (req,res)=>{
//     res.send('hello fhfg ggfhghgf');
// })

mongoose.connect("mongodb://localhost:27017/YourDB", { useNewUrlParser: true , useUnifiedTopology: true, useCreateIndex: true})
    .then(() => console.log('Now connected to MongoDB!'))
    .catch(err => console.error('Something went wrong', err));

var requestTime = (req,res,next)=>{
    req.requestTime = Date.now()
    console.log(req.requestTime)
    next()
}

app.use(requestTime)
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'uploads')));
app.use(cors());
app.use('/api/v1', require('./routes/routes'))
app.use((err,req,res,next)=>{
    if(err.code === "INCORRECT_FILETYPE"){
        return res.status(200).send({success:false,message:'Only Images are allowed',data:[]})
    }else if(err.code === "LIMIT_FILE_SIZE"){
        return res.status(200).send({success:false,message:'Allowed file size 500KB',data:[]})
    }
})

const PORT = process.env.PORT || 3000

app.listen(PORT,()=>{
    console.log(`Listening the Port: ${PORT}`)
})