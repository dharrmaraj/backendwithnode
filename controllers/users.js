const { User } = require('../models/user');

async function users(req,res){
    let Userall = await User.find()
    // const User = mongoose.model('User', Schema({
    //     name: String,
    //     email: String
    //   }));
      
    //   // Empty `filter` means "match all documents"
    //   const filter = {};
    //   const all = await User.find(filter);
    res.send({
        success: true,
        message: 'Users List',
        data: {"Users":Userall}
    })
};

module.exports = users;