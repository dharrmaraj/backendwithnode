const mongoose = require('mongoose')
Schema = mongoose.Schema;

const UploadSchema = new Schema ({
    image_name: {
        type: Buffer,
        required: true,
    },
    id: {
        type: String,
        required: true,
        unique:true,
    },
    file_path:{
        type: String,
        required: true,  
    }
})

exports.Upload = mongoose.model('Upload',UploadSchema);

